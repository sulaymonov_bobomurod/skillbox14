﻿#include <iostream>
#include <string.h>

int main()
{
    std::string name = "Bobomurod";
    std::cout << name.length() << std::endl;
    std::cout << name.at(0) << std::endl;
    std::cout << name.at(name.length() - 1) << std::endl;
}